# in-built
from unittest import TestCase
from unittest.mock import patch
import os
from datetime import datetime
import json

# custom
import url_api

import boto3
import botocore
from moto import mock_dynamodb2


class TestUrlApi(TestCase):
    def get_block_capacity(self, **kwargs):
        return 0

    def get_true(self, **kwargs):
        return True

    def get_list(self, table, mapping, is_true=True):
        return []

    def get_slack_output_valid(self):
        return {
                   "url": "some-url",
                   "status": 200,
                   "message": "done",
                   "input": "ip",
                   "created_at": None,
                   "response_timestamp": datetime.utcnow().strftime(
                       "%Y-%m-%dT%H:%M:%S")
               }, 200

    def get_slack_output_invalid(self):
        return {
                   "url": None,
                   "status": 400,
                   "message": "Invalid URL",
                   "input": None,
                   "created_at": None,
                   "response_timestamp": datetime.utcnow().strftime(
                       "%Y-%m-%dT%H:%M:%S")
               }, 400

    def get_db_record(self, table, mapping, is_true=True):
        return {
            "uuid": 'uuid-101',
            "url": "https://www.google.com",
            "result_url": "http://whyNot.co.uk/starWars",
            "named": True,
            "hash_value": "starWars",
            "hash_id": 1,
            "created_at": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
        }

    def setUp(self):
        self.app = url_api.app.test_client()
        self.app.testing = True
        self.hashed_url = "http://whyNot.co.uk/c3p0"
        self.named_url = "http://whyNot.co.uk/starWars"
        self.table_name = "Thanos"
        self.region_name = "Wakanda"
        self.web_domain_name = "whyNot.co.uk"
        self.api_domain_name = "api.whyNot.co.uk"

        self.env_vars = {
            "API_DOMAIN_NAME": "api.",
            "DYNAMO_TABLE_NAME": self.table_name,
            "DYNAMO_TABLE_REGION": self.region_name,
            "WEB_DOMAIN_NAME": self.web_domain_name,
            "PROTOCOL": "http"
        }

        self.valid_record_1 = {
            "uuid": "-id-",
            "url": self.named_url,
            "result_url": self.hashed_url,
            "named": False,
            "hash_value": "c3p0",
            "hash_id": "hashpcu",
            "created_at": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
        }

        self.valid_record_2 = {
            "uuid": "-id-2",
            "url": self.named_url,
            "result_url": self.hashed_url,
            "named": True,
            "hash_value": "c3p0",
            "hash_id": "hashpcu2",
            "created_at": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
        }

        self.invalid_record = {
            "uuid": "BladeRunner",
            "url": "www.balderunner.com",
            "result_url": "https://Mar-vel.com",
            "hash_value": "Captain-America"
        }

        self.object_args = {
            "table_name": self.env_vars["DYNAMO_TABLE_NAME"],
            "table_region": self.env_vars["DYNAMO_TABLE_REGION"],
            "domain_name": self.env_vars["WEB_DOMAIN_NAME"],
            "protocol": self.env_vars.get("PROTOCOL", "https")
        }

        os.environ.update(self.env_vars)
        self.util = url_api.Util(self.object_args)
        self.valid_mapping_for_scan = {
            "attribute_name": "url",
            "attribute_value": self.named_url
        }

    @patch('url_api.boto3')
    def test_init_function_with_invalid_params(self, mock_boto3):
        self.assertRaises(ValueError, url_api.Util, {})

    @patch('url_api.boto3')
    def test_validate_with_valid_input(self, mock_boto3):
        result = self.util.validate(self.object_args)
        self.assertTrue(result)

    @patch('url_api.boto3')
    def test_validate_with_invalid_input(self, mock_boto3):
        result = self.util.validate({})
        self.assertFalse(result)

    @patch('url_api.boto3')
    def test_calculate_block_capacity_with_valid_inputs(self, mock_boto3):
        response1 = self.util.calculate_block_capacity(1)
        response2 = self.util.calculate_block_capacity(2)
        response3 = self.util.calculate_block_capacity(7)

        result = [response1, response2, response3]

        self.assertTrue(all(result))
        self.assertTrue(all([type(x) == int for x in result]))
        self.assertEqual(result, [9, 90, 9000000])

    @patch('url_api.boto3')
    def test_calculate_block_capacity_with_invalid_inputs(self, mock_boto3):
        self.assertRaises(ValueError, self.util.calculate_block_capacity, 0)
        self.assertRaises(ValueError, self.util.calculate_block_capacity, -1)
        self.assertRaises(ValueError, self.util.calculate_block_capacity, -101)
        self.assertRaises(TypeError, self.util.calculate_block_capacity, None)

    @patch('url_api.boto3')
    def test_get_numeric_digit_with_invalid_inputs(self, mock_boto3):
        self.assertRaises(TypeError, self.util.get_numeric_digit, None, None)
        self.assertRaises(TypeError, self.util.get_numeric_digit, 1, None)
        self.assertRaises(AttributeError, self.util.get_numeric_digit, None, 3)
        self.assertRaises(AttributeError, self.util.get_numeric_digit, 1, 3)
        self.assertRaises(AttributeError, self.util.get_numeric_digit, 1, 3)
        self.assertRaises(ValueError, self.util.get_numeric_digit, -1, -3)

    @patch('url_api.boto3')
    def test_get_numeric_digit_with_valid_inputs(self, mock_boto3):
        response1 = self.util.get_numeric_digit(self.table_name, 1)
        response2 = self.util.get_numeric_digit(self.table_name, 2)
        response3 = self.util.get_numeric_digit(self.table_name, 7)

        result = [response1, response2, response3]

        self.assertTrue(all(result))
        self.assertTrue(all([type(x) == int for x in result]))
        self.assertTrue(response1 < 10)
        self.assertTrue(response2 < 100)
        self.assertTrue(response3 < 1000000)

    @mock_dynamodb2
    def test_scan_by_single_attribute_with_single_record(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        table.put_item(
            Item=self.valid_record_1
        )

        response = self.util.scan_by_single_attribute(
            table, mapping=self.valid_mapping_for_scan)

        self.assertIsInstance(response, dict)
        self.assertEqual(response, self.valid_record_1)

    @mock_dynamodb2
    def test_scan_by_single_attribute_with_multiple_record(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        table.put_item(
            Item=self.valid_record_1
        )

        table.put_item(
            Item=self.valid_record_2
        )

        response = self.util.scan_by_single_attribute(
            table, mapping=self.valid_mapping_for_scan, is_single_record=False)

        self.assertIsInstance(response, list)
        self.assertEqual(response[0], self.valid_record_1)
        self.assertEqual(response[1], self.valid_record_2)

    @mock_dynamodb2
    def test_scan_by_single_attribute_with_no_mapping(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        response = self.util.scan_by_single_attribute(table, mapping=None)

        self.assertIsInstance(response, dict)
        self.assertEqual(response, {})

    @mock_dynamodb2
    def test_scan_by_single_attribute_with_invalid_mapping(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        self.assertRaises(TypeError, self.util.scan_by_single_attribute,
                          table, "WakandaForEver")

    @mock_dynamodb2
    def test_update_record_hash_with_valid_record(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        table.put_item(
            Item=self.valid_record_1
        )

        old_hash_value = self.valid_record_1["hash_value"]
        new_hash_value = "VibraniumisMostPowerfulElement"
        new_url = self.valid_record_1["result_url"].replace(old_hash_value,
                                                            new_hash_value)

        response = self.util.update_record_hash(
            table=table, hash_value=old_hash_value, new_name=new_hash_value,
            new_url=new_url)

        self.assertIsNotNone(response)
        self.assertTrue(response)

    @mock_dynamodb2
    def test_update_record_hash_with_invalid_record(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        old_hash_value = self.invalid_record["hash_value"]
        new_hash_value = "#SharonCarterForEver"
        new_url = self.valid_record_1["result_url"].replace(old_hash_value,
                                                            new_hash_value)

        response = self.util.update_record_hash(
            table=table, hash_value=old_hash_value, new_name=new_hash_value,
            new_url=new_url)

        self.assertIsNotNone(response)
        self.assertFalse(response)

    @mock_dynamodb2
    def test_update_record_hash_with_invalid_inputs(self):
        self.assertRaises(AttributeError, self.util.update_record_hash, None,
                          None, None, None)

        self.assertRaises(AttributeError, self.util.update_record_hash, None,
                          None, None, None)

    @mock_dynamodb2
    def test_persist_records_with_valid_record(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        response = self.util.persist_records(table=table,
                                             record=self.valid_record_1)

        self.assertIsNotNone(response)
        self.assertTrue(response)

    @mock_dynamodb2
    @patch.object(url_api.Util, 'calculate_block_capacity', get_block_capacity)
    def test_persist_records_with_increased_block_size(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        table.put_item(
            Item=self.valid_record_1
        )

        table.put_item(
            Item=self.valid_record_2
        )

        response = self.util.persist_records(table=table,
                                             record=self.valid_record_1)

        self.assertIsNotNone(response)
        self.assertTrue(response)

    @patch('url_api.boto3')
    def test_get_current_block_size(self, mock_boto3):
        response = self.util.get_current_block_size()
        self.assertIsNotNone(response)
        self.assertEqual(response, 3)

    @patch('url_api.boto3')
    def test_update_current_block_size(self, mock_boto3):
        response = self.util.update_current_block_size()
        self.assertIsNotNone(response)
        self.assertEqual(response, 4)

    @mock_dynamodb2
    def test_shorten_url_with_valid_input_existing_record(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        table.put_item(
            Item=self.valid_record_1
        )
        self.util.table = table
        response = self.util.shorten_url(self.named_url)

        self.assertIsNotNone(response)
        self.assertIsInstance(response, dict)
        self.assertEqual(response["result_url"], self.hashed_url)

    @mock_dynamodb2
    def test_shorten_url_with_valid_input_new_record(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        self.util.table = table

        response = self.util.shorten_url(self.named_url)
        self.assertIsNotNone(response)
        self.assertIsInstance(response, dict)
        self.assertTrue(self.web_domain_name in response["result_url"])

    @mock_dynamodb2
    def test_shorten_url_with_invalid_input(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        table.put_item(
            Item=self.valid_record_1
        )
        self.util.table = table
        self.assertRaises(botocore.exceptions.ClientError,
                          self.util.shorten_url, None)

    @mock_dynamodb2
    def test_rename_url_with_valid_input_new_record(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        table.put_item(
            Item=self.valid_record_1
        )
        self.util.table = table
        old_url = self.named_url
        old_hash = self.named_url.split("/")[-1]
        new_hash = "Hulk"
        new_url = self.named_url.replace(old_hash, new_hash)
        response = self.util.rename_url(old_url=old_url, new_url=new_url)

        self.assertIsNotNone(response)
        self.assertIsInstance(response, dict)
        self.assertEqual(response["status"], 200)
        self.assertEqual(response["message"], "successful")
        self.assertEqual(response["requested_name"], new_hash)

    @mock_dynamodb2
    def test_rename_url_with_valid_input_record_exists(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        table.put_item(
            Item=self.valid_record_1
        )
        self.util.table = table
        old_url = self.named_url
        old_hash = self.named_url.split("/")[-1]
        new_hash = "c3p0"
        new_url = self.named_url.replace(old_hash, new_hash)
        response = self.util.rename_url(old_url=old_url, new_url=new_url)

        self.assertIsNotNone(response)
        self.assertIsInstance(response, dict)
        self.assertEqual(response["status"], 400)
        self.assertEqual(response["message"], "Url Name already taken")
        self.assertEqual(response["requested_name"], new_hash)

    @mock_dynamodb2
    def test_rename_url_with_valid_input_more_than_one_record_exists(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName=self.table_name,
            KeySchema=[
                {
                    "AttributeName": "uuid",
                    "KeyType": "HASH"
                }
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "uuid",
                    "AttributeType": "S"
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        table.put_item(
            Item=self.valid_record_1
        )

        table.put_item(
            Item=self.valid_record_2
        )

        self.util.table = table
        old_url = self.named_url
        old_hash = self.named_url.split("/")[-1]
        new_hash = "c3p0"
        new_url = self.named_url.replace(old_hash, new_hash)

        self.assertRaises(ValueError, self.util.rename_url, old_url, new_url)

    @patch('url_api.boto3')
    @patch.object(url_api.Util, 'persist_records', get_true)
    def test_compress_endpoint_get_request(self, mock_boto3):
        response = self.app.get('/compress',
                                query_string={"url": self.named_url})
        result = json.loads(response.get_data(as_text=True))

        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(result["status"], 200)
        self.assertTrue(self.web_domain_name in result["url"])
        self.assertEqual(result["message"], "successful")

    @patch('url_api.boto3')
    @patch.object(url_api.Util, 'persist_records', get_true)
    def test_compress_endpoint_get_request_with_invalid_url_input(self,
                                                                  mock_boto3):
        response = self.app.get('/compress', query_string={"url": None})
        result = json.loads(response.get_data(as_text=True))

        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result["status"], 400)
        self.assertEqual(result["url"], None)
        self.assertEqual(result["message"], "Invalid URL")

    @patch('url_api.boto3')
    @patch.object(url_api.Util, 'persist_records', get_true)
    def test_compress_endpoint_post_request(self, mock_boto3):
        response = self.app.post('/compress',
                                 data=json.dumps({"url": self.named_url}))
        result = json.loads(response.get_data(as_text=True))

        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(result["status"], 200)
        self.assertTrue(self.web_domain_name in result["url"])
        self.assertEqual(result["message"], "successful")

    @patch('url_api.boto3')
    @patch.object(url_api.Util, 'persist_records', get_true)
    def test_compress_endpoint_post_request_with_invalid_url_input(self,
                                                                   mock_boto3):
        response = self.app.post('/compress',
                                 data=json.dumps({"url": None}))
        result = json.loads(response.get_data(as_text=True))

        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result["status"], 400)
        self.assertEqual(result["url"], None)
        self.assertEqual(result["message"], "Invalid URL")

    @patch('url_api.boto3')
    def test_rename_endpoint_get_request(self, mock_boto3):
        response = self.app.get('/rename',
                                query_string={"source": self.named_url,
                                              'target': self.hashed_url})

        result = json.loads(response.get_data(as_text=True))
        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(result["status"], 200)
        self.assertEqual(result["message"], "successful")

    @patch('url_api.boto3')
    def test_rename_endpoint_get_request_with_invalid_source_url(self,
                                                                 mock_boto3):
        response = self.app.get('/rename',
                                query_string={"source": None,
                                              'target': self.hashed_url})

        result = json.loads(response.get_data(as_text=True))

        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result["status"], 400)
        self.assertEqual(result["message"], "Invalid URL")

    @patch('url_api.boto3')
    def test_rename_endpoint_get_request_with_invalid_target_url(self,
                                                                 mock_boto3):
        response = self.app.get('/rename',
                                query_string={"source": self.hashed_url,
                                              'target': None})

        result = json.loads(response.get_data(as_text=True))

        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result["status"], 400)
        self.assertEqual(result["message"], "Invalid URL")

    @patch('url_api.boto3')
    def test_rename_endpoint_post_request(self, mock_boto3):
        response = self.app.post('/rename',
                                 data=json.dumps({"source": self.named_url,
                                                  'target': self.hashed_url}))

        result = json.loads(response.get_data(as_text=True))
        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(result["status"], 200)
        self.assertEqual(result["message"], "successful")

    @patch('url_api.boto3')
    def test_rename_endpoint_post_request_with_invalid_source_url(self,
                                                                  mock_boto3):
        response = self.app.post('/rename',
                                 data=json.dumps({"source": None,
                                                  'target': self.hashed_url}))

        result = json.loads(response.get_data(as_text=True))

        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result["status"], 400)
        self.assertEqual(result["message"], "Invalid URL")

    @patch('url_api.boto3')
    def test_rename_endpoint_post_request_with_invalid_target_url(self,
                                                                  mock_boto3):
        response = self.app.post('/rename',
                                 data=json.dumps({"source": self.named_url,
                                                  'target': None}))

        result = json.loads(response.get_data(as_text=True))

        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result["status"], 400)
        self.assertEqual(result["message"], "Invalid URL")

    @patch('url_api.boto3')
    @patch.object(url_api.Util, 'scan_by_single_attribute', get_db_record)
    def test_hash_redirect_endpoint_with_existing_record(self, mock_boto):
        response = self.app.get("/c3p0")
        result = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(result["status"], 200)
        self.assertEqual(result["message"], "successful")

    @patch('url_api.boto3')
    @patch.object(url_api.Util, 'scan_by_single_attribute', get_list)
    def test_hash_redirect_endpoint_with_inavlid_record(self, mock_boto):
        response = self.app.get("/c3p0")
        result = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 404)
        self.assertEqual(result["status"], 404)
        self.assertEqual(result["message"], "URL not found")
        self.assertEqual(result["created_at"], None)

    @patch('url_api.boto3')
    @patch.object(url_api.Compress, 'get_result_url', get_slack_output_valid)
    def test_slack_integration_with_valid_url(self, mock_boto):
        response = self.app.post("/slack/compress", data={'text': ["inp"]})
        result = response.get_data(as_text=True)
        self.assertIsInstance(result, str)
        self.assertEqual(result.strip(), '"some-url"')

    @patch('url_api.boto3')
    @patch.object(url_api.Compress, 'get_result_url', get_slack_output_invalid)
    def test_slack_integration_with_invalid_url(self, mock_boto):
        response = self.app.post("/slack/compress", data={'text': ["inp"]})
        result = response.get_data(as_text=True)
        self.assertIsInstance(result, str)
        self.assertEqual(result.strip(), '"Invalid URL"')
