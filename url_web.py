# in-built
import os
import logging

# 3rd party
import requests
from flask import (Flask, render_template, redirect, request, url_for, g,
                   Response, flash)

app = Flask(__name__)
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'

logging.getLogger("botocore").setLevel(logging.WARNING)
logging.getLogger().setLevel(logging.INFO)
FORMAT = '%(asctime)s, %(msecs)d %(levelname)-8s [%(lineno)d] %(message)s'

logging.basicConfig(format=FORMAT, datefmt='%d-%m-%Y:%H:%M:%S',
                    level=logging.INFO)


@app.before_request
def before_request():
    g.api_domain_name = os.environ.get("API_DOMAIN_NAME")
    g.protocol = os.environ.get("PROTOCOL", "https")


@app.route('/', methods=["GET", "POST"])
def home_page():
    if request.method == 'POST':
        url = request.form.get('url')
        button_value = request.form.get('msg_button')
        logging.info(request.form)

        if button_value == "Rename":
            logging.debug("Found naming condition", url)
            old_url = request.form.get("previous_url")
            api_url = f"{g.protocol}://{g.api_domain_name}/rename"
            payload = {
                "source": old_url,
                "target": url
            }
            logging.debug(payload)
            api_response = requests.post(api_url, json=payload).json()
            logging.debug(api_response)
            if api_response["status"] == 200:
                flash("Your custom named URL has been created", "success")
                return redirect(url_for('shortened_url',
                                        url=api_response["url"])), 302

            elif api_response["status"] == 400:
                flash(f'Apologies {api_response["url"]} is already taken, '
                      f'try a custom new name', "error")
                return redirect(url_for('shortened_url', url=old_url)), 302

        elif button_value == "Shorten":
            api_url = f"{g.protocol}://{g.api_domain_name}/compress"
            payload = {"url": url}
            api_response = requests.post(api_url, json=payload).json()
            logging.debug(api_response)

            if api_response["status"] == 200:
                return redirect(url_for('shortened_url',
                                        url=api_response["url"])), 302
            else:
                flash(f"Sorry, {api_response['message']}", "error")
                return redirect(url_for('home_page')), 302

    else:
        return Response(render_template("index.html"), mimetype='html'), 200


@app.route('/result')
def shortened_url():
    url = request.args.get('url')
    return Response(render_template('result.html', url=url),
                    mimetype='html'), 200


@app.route('/<hash_value>')
def redirect_url(hash_value):
    api_url = f"{g.protocol}://{g.api_domain_name}/{hash_value}"
    api_response = requests.get(api_url).json()

    if api_response["url"]:
        return redirect(api_response["url"], code=302), 302
    else:
        logging.debug("No Hash found")
        return Response("No Hash Found", mimetype='html'), 404
