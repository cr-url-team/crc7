# in-built
import uuid
from hashlib import sha3_512 as encrypt
import logging
import os
from urllib.parse import urlparse
from datetime import datetime

# 3rd party
import short_url
import boto3
from boto3.dynamodb.conditions import Attr
from flask import (Flask, g, request)
from flask_restful import (Resource, Api)


app = Flask(__name__)
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'
api = Api(app)

FORMAT = '%(asctime)s, %(msecs)d %(levelname)-8s [%(lineno)d] %(message)s'

logging.basicConfig(format=FORMAT, datefmt='%d-%m-%Y:%H:%M:%S',
                    level=logging.INFO)

logging.getLogger("botocore").setLevel(logging.WARNING)
logging.getLogger().setLevel(logging.INFO)


@app.before_request
def before_request():
    args = {
        "table_name": os.environ.get("DYNAMO_TABLE_NAME"),
        "table_region": os.environ.get("DYNAMO_TABLE_REGION"),
        "domain_name": os.environ.get("WEB_DOMAIN_NAME"),
        "protocol": os.environ.get("PROTOCOL", "https")
    }
    logging.info(args)
    logging.debug("g variable set")
    g.handler = Util(args)
    logging.debug(f"g variable {g.handler.protocol}")


class Util:
    def __init__(self, object_args):
        if self.validate(object_args):
            self.db = boto3.resource('dynamodb',
                                     region_name=object_args["table_region"])

            self.table = self.db.Table(object_args["table_name"])

            self.protocol = object_args["protocol"]
            self.domain = object_args["domain_name"]

            self.current_block_size = self.get_current_block_size()
        else:
            raise ValueError("Arguments passed to the object are not valid")

    @staticmethod
    def validate(object_args):
        result = False

        if isinstance(object_args, dict):
            mandatory_keys = {"table_region", "table_name", "protocol",
                              "domain_name"}
            if mandatory_keys - set(object_args.keys()) == set():
                result = True

        return result

    @staticmethod
    def calculate_block_capacity(current_block_size):
        """
         Minimum block size is 1
        :param current_block_size: current config value
        :return:
        """
        if current_block_size == 1:
            return 9

        elif current_block_size > 1:
            max_n_digit = int("".join(['9'] * current_block_size))
            max_nm1_digit = int("".join(['9'] * (current_block_size - 1)))
            return max_n_digit - max_nm1_digit
        else:
            raise ValueError("Invalid block size provided")

    @staticmethod
    def get_numeric_digit(input_string, block_size):
        if block_size > 0:
            return int(encrypt(input_string.encode()).hexdigest(), 32) % (
                    10 ** block_size)
        else:
            raise ValueError("Invalid block size provided")

    @staticmethod
    def scan_by_single_attribute(table, mapping, is_single_record=True):
        result = {}

        if mapping:
            response = table.scan(
                FilterExpression=Attr(mapping["attribute_name"]).eq(
                    mapping["attribute_value"]))["Items"]

            if is_single_record:
                if len(response) == 1:
                    result = response[0]

                elif len(response) == 0:
                    logging.warning(
                        f"No record found for  {mapping['attribute_value']}")
                else:
                    logging.critical(f"""{mapping['attribute_name']}:{mapping[
                        'attribute_value']} has multiple records: #Records={
                    len(response)}""")

                    raise ValueError("More than one record found for same hash"
                                     " value")

            elif not is_single_record:
                result = response

        return result

    @staticmethod
    def update_record_hash(table, hash_value, new_name, new_url):
        logging.info(f"Received record values {(new_name, new_url)}")
        response = table.scan(FilterExpression=Attr(
            "hash_value").eq(hash_value))["Items"]

        if len(response) > 0:
            record_uuid = response[0]["uuid"]
            response = table.update_item(
                Key={
                    'uuid': record_uuid},
                UpdateExpression='SET hash_value = :val1,result_url = :val2,'
                                 'named= :val3',
                ExpressionAttributeValues={
                    ':val1': new_name,
                    ':val2': new_url,
                    ':val3': True})
            logging.debug(response)

            return True
        else:
            return False

    @staticmethod
    def persist_records(table, record):
        if table.item_count > Util.calculate_block_capacity(
                Util.get_current_block_size()):
            Util.update_current_block_size()

        response = table.put_item(Item=record)
        logging.info(response)
        return True

    @staticmethod
    def get_current_block_size():
        return 3

    @staticmethod
    def update_current_block_size():
        return 4

    def shorten_url(self, url):
        exists = self.scan_by_single_attribute(self.table,
                                               {"attribute_name": "url",
                                                "attribute_value": url})
        if exists:
            return exists

        else:
            _id = str(uuid.uuid4().hex)
            numeric_digit = Util.get_numeric_digit(
                input_string=_id, block_size=self.current_block_size)
            hash_value = short_url.encode_url(numeric_digit)
            result_url = f"{self.protocol}://{self.domain}/{hash_value}"
            this_record = {
                "uuid": _id,
                "url": url,
                "result_url": result_url,
                "named": False,
                "hash_value": hash_value,
                "hash_id": numeric_digit,
                "created_at": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
            }

            self.persist_records(table=self.table, record=this_record)
            return this_record

    def rename_url(self, old_url, new_url):
        custom_hash = new_url.split("/")[-1]
        previous_hash = old_url.split("/")[-1]
        exists = self.scan_by_single_attribute(self.table,
                                               {"attribute_name": "hash_value",
                                                "attribute_value": custom_hash}
                                               )
        record = {}
        status = {}

        if len(exists) > 0:
            record = exists
            status = {"requested_name": custom_hash, "status": 400,
                      "message": "Url Name already taken"}

        elif len(exists) == 0:
            self.update_record_hash(table=self.table, hash_value=previous_hash,
                                    new_name=custom_hash, new_url=new_url)
            record = self.scan_by_single_attribute(self.table, {
                "attribute_name": "result_url", "attribute_value": new_url})

            status = {"requested_name": custom_hash, "status": 200,
                      "message": "successful"}
        record.update(status)
        return record


class Compress(Resource):
    def get(self):
        url = request.args.get('url')
        return self.get_result_url(url)

    def post(self):
        json_data = request.get_json(force=True)
        url = json_data.get('url')
        return self.get_result_url(url)

    @staticmethod
    def get_result_url(url):
        parsed = urlparse(url)

        if type(parsed.scheme) != str:
            scheme = parsed.scheme.decode("utf-8")
            netloc = parsed.netloc.decode("utf-8")
        else:
            scheme = parsed.scheme
            netloc = parsed.netloc

        if scheme == '' or netloc == '':
            logging.error("Input URL is invalid")
            data = {
                "url": None,
                "status": 400,
                "message": "Invalid URL",
                "input": str(parsed),
                "created_at": None,
                "response_timestamp": datetime.utcnow().strftime(
                    "%Y-%m-%dT%H:%M:%S")
            }

        else:
            logging.info("Input URL is valid")
            record = g.handler.shorten_url(url)
            data = {
                "url": record.get("result_url"),
                "status": 200,
                "message": "successful",
                "input": str(parsed),
                "created_at": record.get("created_at"),
                "response_timestamp": datetime.utcnow().strftime(
                    "%Y-%m-%dT%H:%M:%S")
            }

        return data, data["status"]


class Rename(Resource):
    def get(self):
        source = request.args.get('source')
        target = request.args.get('target')
        return self.rename_url(source, target)

    def post(self):
        json_data = request.get_json(force=True)
        return self.rename_url(json_data["source"], json_data["target"])

    @staticmethod
    def rename_url(source, target):
        source_parsed = urlparse(source)
        target_parsed = urlparse(target)
        try:
            source_parsed_netloc = source_parsed.netloc.decode("utf-8")
            target_parsed_netloc = target_parsed.netloc.decode("utf-8")
        except AttributeError:
            source_parsed_netloc = source_parsed.netloc
            target_parsed_netloc = target_parsed.netloc

        if source_parsed_netloc == target_parsed_netloc == g.handler.domain:
            record = g.handler.rename_url(old_url=source, new_url=target)
            data = {
                "url": record.get("result_url"),
                "status": record.get("status"),
                "message": record.get("message"),
                "input": {
                    "source": str(source_parsed),
                    "target": str(target_parsed)
                },
                "created_at": record.get("created_at"),
                "response_timestamp": datetime.utcnow().strftime(
                    "%Y-%m-%dT%H:%M:%S")
            }

        else:
            data = {
                "url": None,
                "status": 400,
                "message": "Invalid URL",
                "input": {
                    "source": source,
                    "target": target
                },
                "created_at": None,
                "response_timestamp": datetime.utcnow().strftime(
                    "%Y-%m-%dT%H:%M:%S")
            }

        return data, data["status"]


class GetUrl(Resource):
    def get(self, hash_value):
        record = g.handler.scan_by_single_attribute(g.handler.table, {
            "attribute_name": "hash_value", "attribute_value": hash_value})

        if len(record) > 0:
            data = {
                "url": record.get("url"),
                "status": 200,
                "message": "successful",
                "input": str(urlparse(record.get("url"))),
                "created_at": record.get("created_at"),
                "response_timestamp": datetime.utcnow().strftime(
                    "%Y-%m-%dT%H:%M:%S")
            }

        else:
            data = {
                "url": None,
                "status": 404,
                "message": "URL not found",
                "input": request.url,
                "created_at": None,
                "response_timestamp": datetime.utcnow().strftime(
                    "%Y-%m-%dT%H:%M:%S")
            }

        return data, data["status"]


class SlackCompress(Resource):
    def post(self):
        form_data = request.form
        form_data_as_dict = form_data.to_dict(flat=False)
        url = form_data_as_dict["text"][0]
        response, status = Compress.get_result_url(url)

        if status == 200:
            return response["url"]

        elif status == 400:
            return response["message"]


api.add_resource(Compress, '/compress')
api.add_resource(Rename, '/rename')
api.add_resource(GetUrl, '/<hash_value>')
api.add_resource(SlackCompress, '/slack/compress')
