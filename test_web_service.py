# in-built
from unittest import TestCase
from unittest.mock import patch
import os
from datetime import datetime
from urllib.parse import urlparse

# custom
import url_web


class Object(object):
    pass


class TestWebService(TestCase):
    @staticmethod
    def get_shorten_valid_response():
        return {
            "url": "http://short.uk/",
            "status": 200,
            "message": "successful",
            "input": None,
            "created_at": None,
            "response_timestamp": datetime.utcnow().strftime(
                "%Y-%m-%dT%H:%M:%S")
        }

    @staticmethod
    def get_shorten_invalid_response():
        return {
            "url": None,
            "status": 400,
            "message": "failed",
            "input": None,
            "created_at": None,
            "response_timestamp": datetime.utcnow().strftime(
                "%Y-%m-%dT%H:%M:%S")
        }

    @staticmethod
    def get_rename_valid_response():
        return {
            "url": "target-url",
            "status": 200,
            "message": "done",
            "input": {
                "source": "source-url-parsed",
                "target": "target-url-parsed",
            },
            "created_at": None,
            "response_timestamp": datetime.utcnow().strftime(
                "%Y-%m-%dT%H:%M:%S")
        }

    @staticmethod
    def get_rename_invalid_response():
        return {
            "url": None,
            "status": 400,
            "message": "failed",
            "input": {
                "source": "source-available",
                "target": None,
            },
            "created_at": None,
            "response_timestamp": datetime.utcnow().strftime(
                "%Y-%m-%dT%H:%M:%S")
        }

    def setUp(self):
        self.env_vars = {
            "API_DOMAIN_NAME": "random-domain-name"
        }

        os.environ.update(self.env_vars)

        self.app = url_web.app.test_client()
        self.app.testing = True
        self.shorten_response_valid = Object()
        self.shorten_response_invalid = Object()
        self.shorten_response_valid.json = self.get_shorten_valid_response
        self.shorten_response_invalid.json = self.get_shorten_invalid_response

        self.rename_response = Object()
        self.rename_response.json = self.get_shorten_valid_response
        self.rename_response_invalid = Object()
        self.rename_response_invalid.json = self.get_rename_invalid_response

    def test_home_page_get_request(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.mimetype, "html")

    def test_results_page(self):
        response = self.app.get('/result')
        self.assertEqual(response.status_code, 200)

    @patch("url_web.requests")
    def test_home_page_with_hash_value_invalid_inputs(self, mock_requests):
        mock_requests.get.return_value = self.shorten_response_invalid
        response = self.app.get('/MayTheForceBeWithYou')
        print(response)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.mimetype, "html")
        self.assertEqual(response.data.decode("utf8"), "No Hash Found")

    @patch("url_web.requests")
    def test_home_page_with_hash_value_valid_inputs(self, mock_requests):
        mock_requests.get.return_value = self.shorten_response_valid
        result = self.shorten_response_valid.json()
        response = self.app.get('/MayTheForceBeWithYou')
        print(response)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, result["url"])

    @patch("url_web.requests")
    def test_home_page_with_post_request_for_shortening_url(self,
                                                            mock_requests):

        mock_requests.post.return_value = self.shorten_response_valid
        payload = {"url": "sample_url", "msg_button": "Shorten"}
        response = self.app.post('/', data=payload)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(urlparse(response.location).path, "/result")

    @patch("url_web.requests")
    def test_home_page_post_request_for_shortening_url_with_invalid_values(
            self, mock_requests):

        mock_requests.post.return_value = self.shorten_response_invalid
        payload = {"url": None, "msg_button": "Shorten"}
        response = self.app.post('/', data=payload)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(urlparse(response.location).path, "/")

    @patch("url_web.requests")
    def test_home_page_with_post_request_for_rename_url(self, mock_requests):
        mock_requests.post.return_value = self.rename_response
        payload = {"url": "sample_url", "msg_button": "Rename",
                   "previous_url": "oldUrl"}

        response = self.app.post('/', data=payload)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(urlparse(response.location).path, "/result")

    @patch("url_web.requests")
    def test_home_page_with_post_request_for_rename_url_invalid_inputs(
            self, mock_requests):

        mock_requests.post.return_value = self.rename_response_invalid
        payload = {"url": "sample_url", "msg_button": "Rename",
                   "previous_url": "oldUrl"}

        response = self.app.post('/', data=payload)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(urlparse(response.location).path, "/result")
